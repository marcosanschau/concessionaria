import carrosClass.Carros;
import compraDoCarro.Compra;
import org.junit.Assert;
import org.junit.Test;
import seguroCarro.Seguro;
import situacaoSerasa.Neutro;

public class CompraTest {
    @Test
    public void deveCalcularCorretamenteOPrecoDoCarroSemSeguro() {
        Carros carro = new Carros("Voyage","Wolkswagen",2010,22000,1);
        Neutro neutro = new Neutro("Marcos",24,"12345678978");
        Compra compra = new Compra(carro,neutro);

        double precoEsperado = 22000;

        double precoReal = compra.precoTotalSemSeguro(carro,neutro);

        Assert.assertEquals(precoEsperado,precoReal,0.01);
    }

    @Test
    public void deveCalcularCorretamenteOPrecoDoCarroComSeguro(){
        Carros carro = new Carros("Voyage","Wolkswagen",2010,22000,1);
        Neutro neutro = new Neutro("Marcos",24,"12345678978");
        Seguro seguro = new Seguro();
        Compra compra = new Compra(carro,neutro);

        double precoEsperado = 24200;

        double precoReal = compra.precoTotalComSeguro(carro,neutro,seguro);

        Assert.assertEquals(precoEsperado,precoReal,0.01);
    }

    @Test
    public void deveCalcularCorretamenteOPrecoCom10ParcelasSemSeguro() {
        Carros carro = new Carros("Voyage","Wolkswagen",2010,22000,10);
        Neutro neutro = new Neutro("Marcos",24,"12345678978");
        Compra compra = new Compra(carro,neutro);

        double precoEsperado = 27500;

        double precoReal = compra.precoTotalSemSeguro(carro,neutro);

        Assert.assertEquals(precoEsperado,precoReal,0.01);
    }

    @Test
    public void deveCalcularCorretamenteOPrecoCom10ParcelasComSeguro() {
        Carros carro = new Carros("Voyage","Wolkswagen",2010,22000,10);
        Neutro neutro = new Neutro("Marcos",24,"12345678978");
        Seguro seguro = new Seguro();
        Compra compra = new Compra(carro,neutro);

        double precoEsperado = 29700;

        double precoReal = compra.precoTotalComSeguro(carro,neutro,seguro);

        Assert.assertEquals(precoEsperado,precoReal,0.01);
    }

    @Test
    public void deveCalcularCorretamenteOPrecoDoCarroSemSeguroSeAPessoaTiver50AnosOuMais() {
        Carros carro = new Carros("Voyage","Wolkswagen",2010,22000,1);
        Neutro neutro = new Neutro("Marcos",51,"12345678978");
        Seguro seguro = new Seguro();
        Compra compra = new Compra(carro,neutro);

        double precoEsperado = 24420;

        double precoReal = compra.precoTotalComSeguro(carro,neutro,seguro);

        Assert.assertEquals(precoEsperado,precoReal,0.01);
    }

    @Test
    public void deveCalcularCorretamenteOPrecoDoCarroSemSeguroSeAPessoaMenosDe22Anos() {
        Carros carro = new Carros("Voyage","Wolkswagen",2010,22000,1);
        Neutro neutro = new Neutro("Marcos",21,"12345678978");
        Seguro seguro = new Seguro();
        Compra compra = new Compra(carro,neutro);

        double precoEsperado = 25300;

        double precoReal = compra.precoTotalComSeguro(carro,neutro,seguro);

        Assert.assertEquals(precoEsperado,precoReal,0.01);
    }
}
