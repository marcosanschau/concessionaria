import carrosClass.Carros;
import compraDoCarro.Compra;
import org.junit.Assert;
import org.junit.Test;
import seguroCarro.Seguro;
import situacaoSerasa.Negativo;
import situacaoSerasa.Neutro;
import situacaoSerasa.Pessoa;
import situacaoSerasa.Positivo;

public class SituacaoSerasaTest {
    @Test
    public void deveCalcularCorretamenteSeASituacaoForNegativo() {
        Carros carro = new Carros("Voyage","Wolkswagen",2010,22000,1);
        Negativo negativo = new Negativo("Marcos",24,"12345678978");
        Compra compra = new Compra(carro,negativo);

        double precoEsperado = 25300;

        double precoReal = compra.precoTotalSemSeguro(carro,negativo);

        Assert.assertEquals(precoEsperado,precoReal,0.01);
    }

    @Test
    public void deveCalcularCorretamenteSeASituacaoForNegativoComSeguro() {
        Carros carro = new Carros("Voyage","Wolkswagen",2010,22000,1);
        Negativo negativo = new Negativo("Marcos",24,"12345678978");
        Seguro seguro = new Seguro();
        Compra compra = new Compra(carro,negativo);

        double precoEsperado = 27500;

        double precoReal = compra.precoTotalComSeguro(carro,negativo,seguro);

        Assert.assertEquals(precoEsperado,precoReal,0.01);
    }

    @Test
    public void deveCalcularCorretamenteSeASituacaoForNeutro() {
        Carros carro = new Carros("Voyage","Wolkswagen",2010,22000,1);
        Neutro neutro = new Neutro("Marcos",24,"12345678978");
        Compra compra = new Compra(carro,neutro);

        double precoEsperado = 22000;

        double precoReal = compra.precoTotalSemSeguro(carro,neutro);

        Assert.assertEquals(precoEsperado,precoReal,0.01);
    }

    @Test
    public void deveCalcularCorretamenteSeASituacaoForPositivo() {
        Carros carro = new Carros("Voyage","Wolkswagen",2010,22000,1);
        Positivo positivo = new Positivo("Marcos",24,"12345678978");
        Compra compra = new Compra(carro,positivo);

        double precoEsperado = 19800;

        double precoReal = compra.precoTotalSemSeguro(carro,positivo);

        Assert.assertEquals(precoEsperado,precoReal,0.01);
    }

    @Test
    public void deveCalcularCorretamenteSeForAPessoaPadrao(){
        Carros carro = new Carros("Voyage","Wolkswagen",2010,22000,1);
        Pessoa pessoa = new Pessoa("Marcos",24,"12345678978");
        Compra compra = new Compra(carro,pessoa);

        double precoEsperado = 22000;

        double precoReal = compra.precoTotalSemSeguro(carro,pessoa);

        Assert.assertEquals(precoEsperado,precoReal,0.01);
    }
}
