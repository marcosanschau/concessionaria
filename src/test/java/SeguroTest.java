import carrosClass.Carros;
import compraDoCarro.Compra;
import org.junit.Assert;
import org.junit.Test;
import seguroCarro.Seguro;
import situacaoSerasa.Neutro;

public class SeguroTest {

    @Test
    public void calcularValorDasParcelasDoSeguroCom6Parcelas() {
            Carros carro = new Carros("Voyage","Wolkswagen",2010,22000,1);
            Neutro neutro = new Neutro("Marcos",24,"12345678978");
            Seguro seguro = new Seguro();
            Compra compra = new Compra(carro,neutro);

            double precoEsperado = 366.66;

            double precoReal = seguro.valorDaParcelaDoSeguro(carro,neutro,6);

            Assert.assertEquals(precoEsperado,precoReal,0.01);
    }
}
