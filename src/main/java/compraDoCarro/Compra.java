package compraDoCarro;

import carrosClass.Carros;
import seguroCarro.Seguro;
import situacaoSerasa.Pessoa;

public class Compra {
    private static final double JUROS_POR_PARCELA_AO_MES = 0.025;
    private Pessoa pessoa;
    private Carros carro;
    private Seguro seguro;
    public Compra (Carros carro, Pessoa pessoa){
    }

    public double precoTotalSemSeguro(Carros carro, Pessoa pessoa){
        if(carro.getParcela() == 1){
            return pessoa.precoPagoNoCarroDeAcordoComSituacaoSerasa(carro);
        }
        return pessoa.precoPagoNoCarroDeAcordoComSituacaoSerasa(carro) + (pessoa.precoPagoNoCarroDeAcordoComSituacaoSerasa(carro) * (JUROS_POR_PARCELA_AO_MES * carro.getParcela()));
    }

    public double precoTotalComSeguro(Carros carro, Pessoa pessoa, Seguro seguro){
        return this.precoTotalSemSeguro(carro,pessoa) + seguro.calcularSeguroPorAnoBaseadoEmIdadeDoCompradorEPrecoDoCarro(carro, pessoa);
    }
}
