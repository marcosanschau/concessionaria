package seguroCarro;

import carrosClass.Carros;
import situacaoSerasa.Pessoa;


public class Seguro {
    public static final double MULTIPLICADOR_CALCULO_SEGURO = 0.1;
    private Pessoa pessoa;
    private double multiplicadorSeguroDeAcordoComIdade;
    private double abaixoDaIdade = 1.5;
    private double idadeAdequada = 1.0;
    private double acimaDaIdade = 1.1;
    public double calcularSeguroPorAnoBaseadoEmIdadeDoCompradorEPrecoDoCarro (Carros carros, Pessoa pessoa) {
        final double VALOR_BASE_SEGURO = carros.getPreco() * MULTIPLICADOR_CALCULO_SEGURO;
        int idade = pessoa.getIdade();
        if(idade <= 22) {
            this.multiplicadorSeguroDeAcordoComIdade = this.abaixoDaIdade;
            return VALOR_BASE_SEGURO * this.multiplicadorSeguroDeAcordoComIdade;
        }else if (idade < 50){
            this.multiplicadorSeguroDeAcordoComIdade = this.idadeAdequada;
            return VALOR_BASE_SEGURO * this.multiplicadorSeguroDeAcordoComIdade;
        }
        this.multiplicadorSeguroDeAcordoComIdade = this.acimaDaIdade;
        return VALOR_BASE_SEGURO * this.multiplicadorSeguroDeAcordoComIdade;

    }
    public double valorDaParcelaDoSeguro(Carros carros, Pessoa pessoa, int numeroParcelas) {
        return this.calcularSeguroPorAnoBaseadoEmIdadeDoCompradorEPrecoDoCarro(carros,pessoa) / numeroParcelas;
    }


}
