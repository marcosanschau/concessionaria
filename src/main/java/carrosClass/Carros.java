package carrosClass;

public class Carros {

    private String modelo;
    private String marca;
    private int ano;
    private double preco;
    private int parcela;

    public Carros(final String modelo, final String marca, final int ano, final double preco, final int parcela) {
        this.modelo = modelo;
        this.marca = marca;
        this.ano = ano;
        this.preco = preco;
        this.parcela = parcela;
    }


    public double getPreco() {
        return this.preco;
    }

    public int getParcela() {
        return this.parcela;
    }

}
