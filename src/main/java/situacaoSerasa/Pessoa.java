package situacaoSerasa;

import carrosClass.Carros;

public class Pessoa {
    private static final double MULTIPLICADOR_JUROS_SITUACAO_SERASA = 1.0;
    protected String nome;
    protected int idade;
    protected String cpf;

    public Pessoa(String nome, int idade, String cpf){
        this.nome = nome;
        this.idade = idade;
        this.cpf = cpf;
    }

    public int getIdade() {
        return this.idade;
    }
    public double precoPagoNoCarroDeAcordoComSituacaoSerasa (Carros carro) {
        return carro.getPreco() * MULTIPLICADOR_JUROS_SITUACAO_SERASA;

    }
}
