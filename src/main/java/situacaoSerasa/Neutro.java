package situacaoSerasa;

import carrosClass.Carros;

public class Neutro extends Pessoa{

    private final double MULTIPLICADOR_JUROS_SITUACAO_SERASA = 1.0;

    public Neutro(String nome, int idade, String cpf) {
        super(nome, idade, cpf);
    }
    public double precoPagoNoCarroDeAcordoComSituacaoSerasa (Carros carro) {
        return carro.getPreco() * MULTIPLICADOR_JUROS_SITUACAO_SERASA;

    }
}
