package situacaoSerasa;

import carrosClass.Carros;

public class Positivo extends Pessoa{

    private final double MULTIPLICADOR_JUROS_SITUACAO_SERASA = 0.90;

    public Positivo(String nome, int idade, String cpf) {
        super(nome, idade, cpf);
    }
    public double precoPagoNoCarroDeAcordoComSituacaoSerasa (Carros carro) {
        return carro.getPreco() * MULTIPLICADOR_JUROS_SITUACAO_SERASA;

    }

}
